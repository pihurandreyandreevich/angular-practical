import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.sass']
})
export class DataBindingComponent implements OnInit {

  constructor() { }

  title = 'My Card Title';
  text = 'My sample text';
  number = 42;
  imgUrl = 'https://images.squarespace-cdn.com/content/575a6067b654f9b902f452f4/1552683664433-HFJWSOKJ4NERFD4O8VCE/300Logo.png?content-type=image%2Fpng';
  disabled = false;
  arry = [1, 1, 2, 3, 5, 8, 13];
  obj = {
    name: 'Vladilen',
    info: {
      age: 25,
      job: 'Frontend'
    }
  };

  ngOnInit(): void {
    setTimeout(() => {
      this.imgUrl = 'https://logodix.com/logo/51164.png';
      this.disabled = true;
    }, 3000);
  }

  getInfo(): string {
    return 'This is my info';
  }

  changeTitle(): any {
    this.title = 'Title has been changed!';
  }

  inputHandler(event: any): any {
    console.log(event);
    const value = event.target.value;
    this.title = value;
  }
  inputHandler2(value: any): any {
    this.title = value;
  }
}
