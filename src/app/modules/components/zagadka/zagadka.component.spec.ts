import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZagadkaComponent } from './zagadka.component';

describe('ZagadkaComponent', () => {
  let component: ZagadkaComponent;
  let fixture: ComponentFixture<ZagadkaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZagadkaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZagadkaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
