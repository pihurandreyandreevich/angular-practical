import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-js-learning',
  templateUrl: './js-learning.component.html',
  styleUrls: ['./js-learning.component.sass']
})
export class JsLearningComponent implements OnInit {

  titleArry: any = {
    //количество глав
    chapters: [1, 2, 3],
    //страницы глав
    chapter1: [1.1, 1.2, 1.3, 1.4],
    chapter2: [2.1, 2.2, 2.3, 2.4, 2.5, 2.6],
    chapter3: [3.1, 3.2, 3.3],
  }

  chapter: number = 1  //текущая глава
  keyPage: number = 0  //ключь для текущей страницы 
  page: number = this.titleArry["chapter" + this.chapter][this.keyPage]  //текущая страница

  //следующая глава в массиве
  nextChapter() {
    this.chapter += 1

    //сброс глав на первую
    if (this.chapter == this.titleArry.chapters.length + 1) {
      this.chapter = 1
    }

    //сброс ключа и страницы
    this.keyPage = 0
    this.page = this.titleArry["chapter" + this.chapter][this.keyPage] 
  }

  //предыдущая глава в массиве
  previousChapter() {
    this.chapter -= 1

    //сброс глав на последнюю
    if (this.chapter == 0) {
      this.chapter = this.titleArry.chapters.length
    }

    //сброс ключа и страницы
    this.keyPage = 0
    this.page = this.titleArry["chapter" + this.chapter][this.keyPage] 
  }

  //следующая страница в массиве
  nextPage() {
    this.keyPage += 1
    
    //сброс страницы на первую
    if (this.keyPage == this.titleArry["chapter" + this.chapter].length) {
      this.keyPage = 0
    }
    
    this.page = this.titleArry["chapter" + this.chapter][this.keyPage]
  }

  //предыдущая страница в массиве
  previousPage() {
    this.keyPage -= 1

    //сброс страницы на последнюю
    if (this.keyPage == -1) {
      this.keyPage = this.titleArry["chapter" + this.chapter].length - 1
    }

    this.page = this.titleArry["chapter" + this.chapter][this.keyPage]
  }

  constructor() { }

  ngOnInit(): void {
    console.log("welcome to the club buddy)")
  }

}
