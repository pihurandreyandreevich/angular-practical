import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JsLearningComponent } from './js-learning.component';

describe('JsLearningComponent', () => {
  let component: JsLearningComponent;
  let fixture: ComponentFixture<JsLearningComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JsLearningComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JsLearningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
