import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ViewComponentComponent } from './page/view-component/view-component.component';
import { FirstComponentComponent } from './components/first-component/first-component.component';
import { DataBindingComponent } from './components/data-binding/data-binding.component';
import { ZagadkaComponent } from './components/zagadka/zagadka.component';
import { TooltipComponent } from './components/tooltip/tooltip.component';
import { JsLearningComponent } from './components/js-learning/js-learning.component';



@NgModule({
  declarations: [
  ViewComponentComponent,
  FirstComponentComponent,
  DataBindingComponent,
  ZagadkaComponent,
  TooltipComponent,
  JsLearningComponent
],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    ViewComponentComponent
  ]
})
export class ModulesModule { }
